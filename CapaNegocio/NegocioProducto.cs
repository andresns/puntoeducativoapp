﻿using CapaConexion;
using CapaDTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NegocioProducto
    {
        private Conexion conec1;

        public Conexion Conec1
        {
            get { return conec1; }
            set { conec1 = value; }
        }

        public void configurarConexion()
        {
            this.Conec1 = new Conexion();
            this.Conec1.NombreBaseDeDatos = "PuntoEducativoWS";
            this.Conec1.NombreTabla = "producto";
            this.Conec1.CadenaConexion = "Data Source=DESKTOP-I01RF7O;Initial Catalog=PuntoEducativoWS;Integrated Security=True";

        }

        public DataSet retornaClienteDataSet()
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM producto";
            this.Conec1.EsSelect = true;
            this.Conec1.conectar();

            return this.Conec1.DbDataSet;
        }

        public void ingresarProducto(Producto producto)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "INSERT INTO producto (id_producto,descripcion) " +
                                     " VALUES (" + producto.Id_producto + ",'" +
                                      producto.Descripcion + "');";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();

        }

        public void modificarProducto(Producto producto)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "UPDATE producto set descripcion = '" +
                                     producto.Descripcion + "' WHERE id_producto = " +
                                     producto.Id_producto + ";";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();

        }

        public void eliminarProducto(int idProducto)
        {
            this.configurarConexion();
            this.Conec1.CadenaSQL = "DELETE FROM producto " +
                                    " WHERE id_producto = " + idProducto + ";";
            this.Conec1.EsSelect = false;
            this.Conec1.conectar();

        }

        public List<Producto> obtenerProductos()
        {
            List<Producto> auxListaProducto = new List<Producto>();
            this.configurarConexion();
            this.Conec1.CadenaSQL = "SELECT * FROM producto";
            this.Conec1.EsSelect = true;
            this.Conec1.conectar();

            foreach (DataRow dr in this.Conec1.DbDataSet.Tables[this.Conec1.NombreTabla].Rows)
            {
                Producto auxProducto = new Producto();
                auxProducto.Id_producto = (int)dr["id_producto"];
                auxProducto.Descripcion = (String)dr["descripcion"];
                auxListaProducto.Add(auxProducto);
            } //Fin for


            return auxListaProducto;
        }
    }
}
