﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaGUI
{
    public partial class PantallaPrevisualizarCotizacion : Form
    {
        private string codigoCotizacion;

        public string CodigoCotizacion { get => codigoCotizacion; set => codigoCotizacion = value; }
        public PantallaPrevisualizarCotizacion(string codigoCotizacion)
        {
            InitializeComponent();
            this.CodigoCotizacion = codigoCotizacion;
        }

        public void cargarTablaPrevisualizarCotizacion()
        {
            WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();
            WebServicePuntoEducativo.DetalleCotizacion auxDetalleCotizacion = new WebServicePuntoEducativo.DetalleCotizacion();
            List<WebServicePuntoEducativo.DetalleCotizacion> auxListaDetalleCotizaciones = new List<WebServicePuntoEducativo.DetalleCotizacion>();

            auxListaDetalleCotizaciones = auxWSPuntoEducativo.obtenerDetalleCotizacionCodigo(this.CodigoCotizacion).ToList();

            DataTable auxDataTable = new DataTable();
            auxDataTable.Columns.Add("Id", typeof(int));
            auxDataTable.Columns.Add("Descripcion", typeof(string));
            auxDataTable.Columns.Add("Cantidad", typeof(int));
            auxDataTable.Columns.Add("Precio", typeof(int));

            dgDetalleCotizacion.DataSource = auxDataTable;

            foreach (WebServicePuntoEducativo.DetalleCotizacion dc in auxListaDetalleCotizaciones)
            {
                WebServicePuntoEducativo.Producto auxProducto = new WebServicePuntoEducativo.Producto();

                auxProducto = auxWSPuntoEducativo.buscarProductoId(dc.IdProducto);
                DataRow auxDataRow = auxDataTable.NewRow();
                auxDataRow["Id"] = dc.IdProducto;
                auxDataRow["Descripcion"] = auxProducto.Descripcion;
                auxDataRow["Cantidad"] = dc.Cantidad;
                auxDataRow["Precio"] = dc.PrecioUnitario;
                auxDataTable.Rows.Add(auxDataRow);
            }

            dgDetalleCotizacion.Columns["Id"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgDetalleCotizacion.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgDetalleCotizacion.Columns["Id"].ReadOnly = true;
            dgDetalleCotizacion.Columns["Descripcion"].ReadOnly = true;
            dgDetalleCotizacion.Columns["Cantidad"].ReadOnly = true;
            dgDetalleCotizacion.Columns["Precio"].ReadOnly = true;

            dgDetalleCotizacion.ClearSelection();
        }

        private void PantallaPrevisualizarCotizacion_Load(object sender, EventArgs e)
        {
            cargarTablaPrevisualizarCotizacion();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Dispose();
            System.GC.Collect();
        }

        private void BtnModificarDetalleCotizacion_Click(object sender, EventArgs e)
        {
            WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();
            WebServicePuntoEducativo.DetalleCotizacion auxDetalleCotizacion = new WebServicePuntoEducativo.DetalleCotizacion();

            int indiceFila = dgDetalleCotizacion.SelectedCells[0].RowIndex;
            DataGridViewRow filaSeleccionada = dgDetalleCotizacion.Rows[indiceFila];

            int idProducto = int.Parse(filaSeleccionada.Cells["Id"].Value.ToString());
            int cantidad = int.Parse(filaSeleccionada.Cells["Cantidad"].Value.ToString());
            int precio = int.Parse(filaSeleccionada.Cells["Precio"].Value.ToString());

            auxDetalleCotizacion.IdProducto = idProducto;
            auxDetalleCotizacion.CodCotizacion = this.codigoCotizacion;
            auxDetalleCotizacion.Cantidad = cantidad;
            auxDetalleCotizacion.PrecioUnitario = precio;

            PantallaModificarDetalleCotizacion pDetalleCotizacion = new PantallaModificarDetalleCotizacion(this, auxDetalleCotizacion);
            pDetalleCotizacion.ShowDialog();

            auxWSPuntoEducativo.modificarDetalleCotizacion(auxDetalleCotizacion);
        }

        private void BtnEliminarDetalleCotizacion_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("¿Está seguro de que desea eliminar la línea?", "Confirmación", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();

                int indiceFila = dgDetalleCotizacion.SelectedCells[0].RowIndex;
                DataGridViewRow filaSeleccionada = dgDetalleCotizacion.Rows[indiceFila];

                int idProducto = int.Parse(filaSeleccionada.Cells["Id"].Value.ToString());

                auxWSPuntoEducativo.eliminarDetalleCotizacion(idProducto, this.CodigoCotizacion);
                cargarTablaPrevisualizarCotizacion();
            }
                
        }

        private void DgDetalleCotizacion_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnEliminarDetalleCotizacion.Enabled = true;
            btnModificarDetalleCotizacion.Enabled = true;
        }
    }
}
