﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaGUI.WebServicePuntoEducativo;

namespace CapaGUI
{
    public partial class PantallaModificarDetalleCotizacion : Form
    {
        private WebServicePuntoEducativo.DetalleCotizacion detalleCotizacion;
        private PantallaPrevisualizarCotizacion pPrevisualizarDetalleCotizacion;
        public DetalleCotizacion DetalleCotizacion { get => detalleCotizacion; set => detalleCotizacion = value; }
        public PantallaPrevisualizarCotizacion PPrevisualizarDetalleCotizacion { get => pPrevisualizarDetalleCotizacion; set => pPrevisualizarDetalleCotizacion = value; }

        public PantallaModificarDetalleCotizacion(PantallaPrevisualizarCotizacion pPrevisualizarDetalleCotizacion, WebServicePuntoEducativo.DetalleCotizacion detalleCotizacion)
        {
            InitializeComponent();
            this.DetalleCotizacion = detalleCotizacion;
            this.PPrevisualizarDetalleCotizacion = pPrevisualizarDetalleCotizacion;
        }

        private void PantallaModificarDetalleCotizacion_Load(object sender, EventArgs e)
        {
            txtCantidad.Text = this.detalleCotizacion.Cantidad.ToString();
            txtPrecio.Text = this.detalleCotizacion.PrecioUnitario.ToString();
        }

        private void BtnGuardarDetalleCotizacion_Click(object sender, EventArgs e)
        {
            WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();
            this.DetalleCotizacion.Cantidad = int.Parse(txtCantidad.Text);
            this.DetalleCotizacion.PrecioUnitario = int.Parse(txtPrecio.Text);

            auxWSPuntoEducativo.modificarDetalleCotizacion(this.DetalleCotizacion);

            MessageBox.Show("Se han guardado los cambios.", "Sistema");

            this.Dispose();
            this.PPrevisualizarDetalleCotizacion.cargarTablaPrevisualizarCotizacion();
            System.GC.Collect();
        }
    }
}
