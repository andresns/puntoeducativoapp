﻿namespace CapaGUI
{
    partial class PantallaPrevisualizarCotizacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgDetalleCotizacion = new System.Windows.Forms.DataGridView();
            this.btnModificarDetalleCotizacion = new System.Windows.Forms.Button();
            this.btnEliminarDetalleCotizacion = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetalleCotizacion)).BeginInit();
            this.SuspendLayout();
            // 
            // dgDetalleCotizacion
            // 
            this.dgDetalleCotizacion.AllowUserToAddRows = false;
            this.dgDetalleCotizacion.AllowUserToDeleteRows = false;
            this.dgDetalleCotizacion.AllowUserToResizeColumns = false;
            this.dgDetalleCotizacion.AllowUserToResizeRows = false;
            this.dgDetalleCotizacion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDetalleCotizacion.Location = new System.Drawing.Point(28, 24);
            this.dgDetalleCotizacion.Name = "dgDetalleCotizacion";
            this.dgDetalleCotizacion.ReadOnly = true;
            this.dgDetalleCotizacion.RowHeadersVisible = false;
            this.dgDetalleCotizacion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDetalleCotizacion.Size = new System.Drawing.Size(737, 314);
            this.dgDetalleCotizacion.TabIndex = 9;
            this.dgDetalleCotizacion.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DgDetalleCotizacion_CellMouseClick);
            // 
            // btnModificarDetalleCotizacion
            // 
            this.btnModificarDetalleCotizacion.Enabled = false;
            this.btnModificarDetalleCotizacion.Location = new System.Drawing.Point(623, 355);
            this.btnModificarDetalleCotizacion.Name = "btnModificarDetalleCotizacion";
            this.btnModificarDetalleCotizacion.Size = new System.Drawing.Size(142, 23);
            this.btnModificarDetalleCotizacion.TabIndex = 10;
            this.btnModificarDetalleCotizacion.Text = "Modificar";
            this.btnModificarDetalleCotizacion.UseVisualStyleBackColor = true;
            this.btnModificarDetalleCotizacion.Click += new System.EventHandler(this.BtnModificarDetalleCotizacion_Click);
            // 
            // btnEliminarDetalleCotizacion
            // 
            this.btnEliminarDetalleCotizacion.Enabled = false;
            this.btnEliminarDetalleCotizacion.Location = new System.Drawing.Point(475, 355);
            this.btnEliminarDetalleCotizacion.Name = "btnEliminarDetalleCotizacion";
            this.btnEliminarDetalleCotizacion.Size = new System.Drawing.Size(142, 23);
            this.btnEliminarDetalleCotizacion.TabIndex = 11;
            this.btnEliminarDetalleCotizacion.Text = "Eliminar";
            this.btnEliminarDetalleCotizacion.UseVisualStyleBackColor = true;
            this.btnEliminarDetalleCotizacion.Click += new System.EventHandler(this.BtnEliminarDetalleCotizacion_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Location = new System.Drawing.Point(28, 355);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(142, 23);
            this.btnCerrar.TabIndex = 12;
            this.btnCerrar.Text = "Cerrar";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // PantallaPrevisualizarCotizacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 397);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.btnEliminarDetalleCotizacion);
            this.Controls.Add(this.btnModificarDetalleCotizacion);
            this.Controls.Add(this.dgDetalleCotizacion);
            this.Name = "PantallaPrevisualizarCotizacion";
            this.Text = "PantallaPrevisualizarCotizacion";
            this.Load += new System.EventHandler(this.PantallaPrevisualizarCotizacion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgDetalleCotizacion)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgDetalleCotizacion;
        private System.Windows.Forms.Button btnModificarDetalleCotizacion;
        private System.Windows.Forms.Button btnEliminarDetalleCotizacion;
        private System.Windows.Forms.Button btnCerrar;
    }
}