﻿namespace CapaGUI
{
    partial class PantallaInicio
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCrearProducto = new System.Windows.Forms.Button();
            this.txtDescripcionProducto = new System.Windows.Forms.TextBox();
            this.txtIdProducto = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.dgTablaProductos = new System.Windows.Forms.DataGridView();
            this.rbBuscarId = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbBuscarDescripcion = new System.Windows.Forms.RadioButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabCRUD = new System.Windows.Forms.TabPage();
            this.tabCrearCotizacion = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnNuevoProveedor = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnCargarDatosProveedor = new System.Windows.Forms.Button();
            this.cbxProveedores = new System.Windows.Forms.ComboBox();
            this.btnPrevisualizarCotizacion = new System.Windows.Forms.Button();
            this.btnFinalizarCotizacion = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dgDetalleOrdenCompra = new System.Windows.Forms.DataGridView();
            this.btnAgregarACotizacion = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtRut = new System.Windows.Forms.TextBox();
            this.btnModificarProveedor = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtRazonSocial = new System.Windows.Forms.TextBox();
            this.btnNuevaCotizacion = new System.Windows.Forms.Button();
            this.txtDV = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabVerCotizaciones = new System.Windows.Forms.TabPage();
            this.tabDetalleOC = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.dgTablaCotizaciones = new System.Windows.Forms.DataGridView();
            this.btnVerCotizacion = new System.Windows.Forms.Button();
            this.btnEliminarCotizacion = new System.Windows.Forms.Button();
            this.cbxProveedoresCotizacion = new System.Windows.Forms.ComboBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnCargarCotizaciones = new System.Windows.Forms.Button();
            this.rbTodosProveedores = new System.Windows.Forms.RadioButton();
            this.rbSeleccionarProveedor = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTablaProductos)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabCRUD.SuspendLayout();
            this.tabCrearCotizacion.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetalleOrdenCompra)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.tabVerCotizaciones.SuspendLayout();
            this.tabDetalleOC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTablaCotizaciones)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCrearProducto);
            this.groupBox1.Controls.Add(this.txtDescripcionProducto);
            this.groupBox1.Controls.Add(this.txtIdProducto);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(578, 80);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nuevo Producto";
            // 
            // btnCrearProducto
            // 
            this.btnCrearProducto.Location = new System.Drawing.Point(447, 30);
            this.btnCrearProducto.Name = "btnCrearProducto";
            this.btnCrearProducto.Size = new System.Drawing.Size(125, 23);
            this.btnCrearProducto.TabIndex = 4;
            this.btnCrearProducto.Text = "Crear Producto";
            this.btnCrearProducto.UseVisualStyleBackColor = true;
            this.btnCrearProducto.Click += new System.EventHandler(this.BtnCrearProducto_Click);
            // 
            // txtDescripcionProducto
            // 
            this.txtDescripcionProducto.Location = new System.Drawing.Point(188, 32);
            this.txtDescripcionProducto.Name = "txtDescripcionProducto";
            this.txtDescripcionProducto.Size = new System.Drawing.Size(253, 20);
            this.txtDescripcionProducto.TabIndex = 3;
            // 
            // txtIdProducto
            // 
            this.txtIdProducto.Location = new System.Drawing.Point(47, 32);
            this.txtIdProducto.Name = "txtIdProducto";
            this.txtIdProducto.Size = new System.Drawing.Size(48, 20);
            this.txtIdProducto.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(116, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Descripción:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID:";
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(139, 34);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(302, 20);
            this.txtBuscar.TabIndex = 5;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(447, 32);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(125, 23);
            this.btnBuscar.TabIndex = 6;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.BtnBuscar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(453, 533);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(128, 23);
            this.btnEliminar.TabIndex = 9;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.BtnEliminar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(319, 533);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(128, 23);
            this.btnModificar.TabIndex = 8;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.BtnModificar_Click);
            // 
            // dgTablaProductos
            // 
            this.dgTablaProductos.AllowUserToAddRows = false;
            this.dgTablaProductos.AllowUserToDeleteRows = false;
            this.dgTablaProductos.AllowUserToResizeColumns = false;
            this.dgTablaProductos.AllowUserToResizeRows = false;
            this.dgTablaProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTablaProductos.Location = new System.Drawing.Point(6, 197);
            this.dgTablaProductos.Name = "dgTablaProductos";
            this.dgTablaProductos.ReadOnly = true;
            this.dgTablaProductos.RowHeadersVisible = false;
            this.dgTablaProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTablaProductos.Size = new System.Drawing.Size(573, 320);
            this.dgTablaProductos.TabIndex = 7;
            this.dgTablaProductos.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DgTablaResultados_CellMouseClick);
            // 
            // rbBuscarId
            // 
            this.rbBuscarId.AutoSize = true;
            this.rbBuscarId.Checked = true;
            this.rbBuscarId.Location = new System.Drawing.Point(10, 35);
            this.rbBuscarId.Name = "rbBuscarId";
            this.rbBuscarId.Size = new System.Drawing.Size(36, 17);
            this.rbBuscarId.TabIndex = 12;
            this.rbBuscarId.TabStop = true;
            this.rbBuscarId.Text = "ID";
            this.rbBuscarId.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbBuscarDescripcion);
            this.groupBox2.Controls.Add(this.rbBuscarId);
            this.groupBox2.Controls.Add(this.txtBuscar);
            this.groupBox2.Controls.Add(this.btnBuscar);
            this.groupBox2.Location = new System.Drawing.Point(6, 102);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(578, 80);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Buscar";
            // 
            // rbBuscarDescripcion
            // 
            this.rbBuscarDescripcion.AutoSize = true;
            this.rbBuscarDescripcion.Location = new System.Drawing.Point(52, 35);
            this.rbBuscarDescripcion.Name = "rbBuscarDescripcion";
            this.rbBuscarDescripcion.Size = new System.Drawing.Size(81, 17);
            this.rbBuscarDescripcion.TabIndex = 13;
            this.rbBuscarDescripcion.TabStop = true;
            this.rbBuscarDescripcion.Text = "Descripción";
            this.rbBuscarDescripcion.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabCRUD);
            this.tabControl1.Controls.Add(this.tabCrearCotizacion);
            this.tabControl1.Controls.Add(this.tabVerCotizaciones);
            this.tabControl1.Controls.Add(this.tabDetalleOC);
            this.tabControl1.Location = new System.Drawing.Point(9, 9);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(599, 598);
            this.tabControl1.TabIndex = 14;
            // 
            // tabCRUD
            // 
            this.tabCRUD.Controls.Add(this.groupBox1);
            this.tabCRUD.Controls.Add(this.groupBox2);
            this.tabCRUD.Controls.Add(this.dgTablaProductos);
            this.tabCRUD.Controls.Add(this.btnEliminar);
            this.tabCRUD.Controls.Add(this.btnModificar);
            this.tabCRUD.Location = new System.Drawing.Point(4, 22);
            this.tabCRUD.Name = "tabCRUD";
            this.tabCRUD.Padding = new System.Windows.Forms.Padding(3);
            this.tabCRUD.Size = new System.Drawing.Size(591, 572);
            this.tabCRUD.TabIndex = 0;
            this.tabCRUD.Text = "CRUD Productos";
            this.tabCRUD.UseVisualStyleBackColor = true;
            // 
            // tabCrearCotizacion
            // 
            this.tabCrearCotizacion.Controls.Add(this.groupBox6);
            this.tabCrearCotizacion.Controls.Add(this.groupBox4);
            this.tabCrearCotizacion.Controls.Add(this.btnPrevisualizarCotizacion);
            this.tabCrearCotizacion.Controls.Add(this.btnFinalizarCotizacion);
            this.tabCrearCotizacion.Controls.Add(this.groupBox5);
            this.tabCrearCotizacion.Controls.Add(this.groupBox3);
            this.tabCrearCotizacion.Location = new System.Drawing.Point(4, 22);
            this.tabCrearCotizacion.Name = "tabCrearCotizacion";
            this.tabCrearCotizacion.Padding = new System.Windows.Forms.Padding(3);
            this.tabCrearCotizacion.Size = new System.Drawing.Size(591, 572);
            this.tabCrearCotizacion.TabIndex = 1;
            this.tabCrearCotizacion.Text = "Crear Cotización";
            this.tabCrearCotizacion.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnNuevoProveedor);
            this.groupBox6.Location = new System.Drawing.Point(352, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(233, 89);
            this.groupBox6.TabIndex = 16;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Nuevo Proveedor";
            // 
            // btnNuevoProveedor
            // 
            this.btnNuevoProveedor.Location = new System.Drawing.Point(51, 37);
            this.btnNuevoProveedor.Name = "btnNuevoProveedor";
            this.btnNuevoProveedor.Size = new System.Drawing.Size(138, 23);
            this.btnNuevoProveedor.TabIndex = 15;
            this.btnNuevoProveedor.Text = "Crear Proveedor";
            this.btnNuevoProveedor.UseVisualStyleBackColor = true;
            this.btnNuevoProveedor.Click += new System.EventHandler(this.BtnNuevoProveedor_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnCargarDatosProveedor);
            this.groupBox4.Controls.Add(this.cbxProveedores);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(340, 89);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Proveedor Existente";
            // 
            // btnCargarDatosProveedor
            // 
            this.btnCargarDatosProveedor.Location = new System.Drawing.Point(196, 37);
            this.btnCargarDatosProveedor.Name = "btnCargarDatosProveedor";
            this.btnCargarDatosProveedor.Size = new System.Drawing.Size(125, 23);
            this.btnCargarDatosProveedor.TabIndex = 17;
            this.btnCargarDatosProveedor.Text = "Cargar Datos";
            this.btnCargarDatosProveedor.UseVisualStyleBackColor = true;
            this.btnCargarDatosProveedor.Click += new System.EventHandler(this.BtnCargarDatosProveedor_Click);
            // 
            // cbxProveedores
            // 
            this.cbxProveedores.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxProveedores.FormattingEnabled = true;
            this.cbxProveedores.Location = new System.Drawing.Point(11, 37);
            this.cbxProveedores.Name = "cbxProveedores";
            this.cbxProveedores.Size = new System.Drawing.Size(160, 21);
            this.cbxProveedores.TabIndex = 16;
            // 
            // btnPrevisualizarCotizacion
            // 
            this.btnPrevisualizarCotizacion.Enabled = false;
            this.btnPrevisualizarCotizacion.Location = new System.Drawing.Point(295, 536);
            this.btnPrevisualizarCotizacion.Name = "btnPrevisualizarCotizacion";
            this.btnPrevisualizarCotizacion.Size = new System.Drawing.Size(139, 23);
            this.btnPrevisualizarCotizacion.TabIndex = 17;
            this.btnPrevisualizarCotizacion.Text = "Previsualizar Cotización";
            this.btnPrevisualizarCotizacion.UseVisualStyleBackColor = true;
            this.btnPrevisualizarCotizacion.Click += new System.EventHandler(this.BtnPrevisualizarCotizacion_Click);
            // 
            // btnFinalizarCotizacion
            // 
            this.btnFinalizarCotizacion.Enabled = false;
            this.btnFinalizarCotizacion.Location = new System.Drawing.Point(440, 536);
            this.btnFinalizarCotizacion.Name = "btnFinalizarCotizacion";
            this.btnFinalizarCotizacion.Size = new System.Drawing.Size(139, 23);
            this.btnFinalizarCotizacion.TabIndex = 16;
            this.btnFinalizarCotizacion.Text = "Finalizar Cotización";
            this.btnFinalizarCotizacion.UseVisualStyleBackColor = true;
            this.btnFinalizarCotizacion.Click += new System.EventHandler(this.BtnFinalizarCotizacion_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dgDetalleOrdenCompra);
            this.groupBox5.Controls.Add(this.btnAgregarACotizacion);
            this.groupBox5.Location = new System.Drawing.Point(6, 307);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(579, 213);
            this.groupBox5.TabIndex = 12;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Productos";
            // 
            // dgDetalleOrdenCompra
            // 
            this.dgDetalleOrdenCompra.AllowUserToAddRows = false;
            this.dgDetalleOrdenCompra.AllowUserToDeleteRows = false;
            this.dgDetalleOrdenCompra.AllowUserToResizeColumns = false;
            this.dgDetalleOrdenCompra.AllowUserToResizeRows = false;
            this.dgDetalleOrdenCompra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDetalleOrdenCompra.Location = new System.Drawing.Point(6, 19);
            this.dgDetalleOrdenCompra.Name = "dgDetalleOrdenCompra";
            this.dgDetalleOrdenCompra.ReadOnly = true;
            this.dgDetalleOrdenCompra.RowHeadersVisible = false;
            this.dgDetalleOrdenCompra.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDetalleOrdenCompra.Size = new System.Drawing.Size(567, 135);
            this.dgDetalleOrdenCompra.TabIndex = 8;
            this.dgDetalleOrdenCompra.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DgDetalleOrdenCompra_CellMouseClick);
            // 
            // btnAgregarACotizacion
            // 
            this.btnAgregarACotizacion.Enabled = false;
            this.btnAgregarACotizacion.Location = new System.Drawing.Point(305, 170);
            this.btnAgregarACotizacion.Name = "btnAgregarACotizacion";
            this.btnAgregarACotizacion.Size = new System.Drawing.Size(248, 23);
            this.btnAgregarACotizacion.TabIndex = 9;
            this.btnAgregarACotizacion.Text = "Agregar Producto a Cotización";
            this.btnAgregarACotizacion.UseVisualStyleBackColor = true;
            this.btnAgregarACotizacion.Click += new System.EventHandler(this.BtnAgregarACotizacion_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtRut);
            this.groupBox3.Controls.Add(this.btnModificarProveedor);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.txtRazonSocial);
            this.groupBox3.Controls.Add(this.btnNuevaCotizacion);
            this.groupBox3.Controls.Add(this.txtDV);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtEmail);
            this.groupBox3.Controls.Add(this.txtDireccion);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.txtTelefono);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(6, 101);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(579, 200);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Datos Proveedor";
            // 
            // txtRut
            // 
            this.txtRut.Enabled = false;
            this.txtRut.Location = new System.Drawing.Point(11, 42);
            this.txtRut.MaxLength = 1;
            this.txtRut.Name = "txtRut";
            this.txtRut.Size = new System.Drawing.Size(93, 20);
            this.txtRut.TabIndex = 19;
            // 
            // btnModificarProveedor
            // 
            this.btnModificarProveedor.Enabled = false;
            this.btnModificarProveedor.Location = new System.Drawing.Point(270, 166);
            this.btnModificarProveedor.Name = "btnModificarProveedor";
            this.btnModificarProveedor.Size = new System.Drawing.Size(138, 23);
            this.btnModificarProveedor.TabIndex = 18;
            this.btnModificarProveedor.Text = "Modificar Datos";
            this.btnModificarProveedor.UseVisualStyleBackColor = true;
            this.btnModificarProveedor.Click += new System.EventHandler(this.BtnModificarProveedor_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(108, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "-";
            // 
            // txtRazonSocial
            // 
            this.txtRazonSocial.Enabled = false;
            this.txtRazonSocial.Location = new System.Drawing.Point(165, 42);
            this.txtRazonSocial.Name = "txtRazonSocial";
            this.txtRazonSocial.Size = new System.Drawing.Size(408, 20);
            this.txtRazonSocial.TabIndex = 6;
            // 
            // btnNuevaCotizacion
            // 
            this.btnNuevaCotizacion.Enabled = false;
            this.btnNuevaCotizacion.Location = new System.Drawing.Point(425, 166);
            this.btnNuevaCotizacion.Name = "btnNuevaCotizacion";
            this.btnNuevaCotizacion.Size = new System.Drawing.Size(139, 23);
            this.btnNuevaCotizacion.TabIndex = 0;
            this.btnNuevaCotizacion.Text = "Nueva Cotización";
            this.btnNuevaCotizacion.UseVisualStyleBackColor = true;
            this.btnNuevaCotizacion.Click += new System.EventHandler(this.BtnNuevaCotizacion_Click);
            // 
            // txtDV
            // 
            this.txtDV.Enabled = false;
            this.txtDV.Location = new System.Drawing.Point(121, 42);
            this.txtDV.MaxLength = 1;
            this.txtDV.Name = "txtDV";
            this.txtDV.Size = new System.Drawing.Size(18, 20);
            this.txtDV.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Rut:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Teléfono:";
            // 
            // txtEmail
            // 
            this.txtEmail.Enabled = false;
            this.txtEmail.Location = new System.Drawing.Point(165, 140);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(408, 20);
            this.txtEmail.TabIndex = 12;
            // 
            // txtDireccion
            // 
            this.txtDireccion.Enabled = false;
            this.txtDireccion.Location = new System.Drawing.Point(11, 92);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(562, 20);
            this.txtDireccion.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(162, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Razón Social:";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Enabled = false;
            this.txtTelefono.Location = new System.Drawing.Point(11, 140);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(128, 20);
            this.txtTelefono.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(162, 124);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Email:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Dirección:";
            // 
            // tabVerCotizaciones
            // 
            this.tabVerCotizaciones.Controls.Add(this.groupBox7);
            this.tabVerCotizaciones.Controls.Add(this.btnEliminarCotizacion);
            this.tabVerCotizaciones.Controls.Add(this.btnVerCotizacion);
            this.tabVerCotizaciones.Controls.Add(this.dgTablaCotizaciones);
            this.tabVerCotizaciones.Location = new System.Drawing.Point(4, 22);
            this.tabVerCotizaciones.Name = "tabVerCotizaciones";
            this.tabVerCotizaciones.Size = new System.Drawing.Size(591, 572);
            this.tabVerCotizaciones.TabIndex = 2;
            this.tabVerCotizaciones.Text = "Ver Cotizaciones";
            this.tabVerCotizaciones.UseVisualStyleBackColor = true;
            // 
            // tabDetalleOC
            // 
            this.tabDetalleOC.Controls.Add(this.button1);
            this.tabDetalleOC.Location = new System.Drawing.Point(4, 22);
            this.tabDetalleOC.Name = "tabDetalleOC";
            this.tabDetalleOC.Size = new System.Drawing.Size(591, 572);
            this.tabDetalleOC.TabIndex = 3;
            this.tabDetalleOC.Text = "DetalleOC";
            this.tabDetalleOC.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(224, 94);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // dgTablaCotizaciones
            // 
            this.dgTablaCotizaciones.AllowUserToAddRows = false;
            this.dgTablaCotizaciones.AllowUserToDeleteRows = false;
            this.dgTablaCotizaciones.AllowUserToResizeColumns = false;
            this.dgTablaCotizaciones.AllowUserToResizeRows = false;
            this.dgTablaCotizaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTablaCotizaciones.Location = new System.Drawing.Point(3, 72);
            this.dgTablaCotizaciones.Name = "dgTablaCotizaciones";
            this.dgTablaCotizaciones.ReadOnly = true;
            this.dgTablaCotizaciones.RowHeadersVisible = false;
            this.dgTablaCotizaciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgTablaCotizaciones.Size = new System.Drawing.Size(585, 444);
            this.dgTablaCotizaciones.TabIndex = 9;
            // 
            // btnVerCotizacion
            // 
            this.btnVerCotizacion.Enabled = false;
            this.btnVerCotizacion.Location = new System.Drawing.Point(443, 533);
            this.btnVerCotizacion.Name = "btnVerCotizacion";
            this.btnVerCotizacion.Size = new System.Drawing.Size(136, 23);
            this.btnVerCotizacion.TabIndex = 10;
            this.btnVerCotizacion.Text = "Ver Cotización";
            this.btnVerCotizacion.UseVisualStyleBackColor = true;
            this.btnVerCotizacion.Click += new System.EventHandler(this.BtnVerCotizacion_Click);
            // 
            // btnEliminarCotizacion
            // 
            this.btnEliminarCotizacion.Enabled = false;
            this.btnEliminarCotizacion.Location = new System.Drawing.Point(289, 533);
            this.btnEliminarCotizacion.Name = "btnEliminarCotizacion";
            this.btnEliminarCotizacion.Size = new System.Drawing.Size(136, 23);
            this.btnEliminarCotizacion.TabIndex = 11;
            this.btnEliminarCotizacion.Text = "Eliminar Cotización";
            this.btnEliminarCotizacion.UseVisualStyleBackColor = true;
            this.btnEliminarCotizacion.Click += new System.EventHandler(this.BtnEliminarCotizacion_Click);
            // 
            // cbxProveedoresCotizacion
            // 
            this.cbxProveedoresCotizacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxProveedoresCotizacion.Enabled = false;
            this.cbxProveedoresCotizacion.FormattingEnabled = true;
            this.cbxProveedoresCotizacion.Location = new System.Drawing.Point(237, 24);
            this.cbxProveedoresCotizacion.Name = "cbxProveedoresCotizacion";
            this.cbxProveedoresCotizacion.Size = new System.Drawing.Size(160, 21);
            this.cbxProveedoresCotizacion.TabIndex = 17;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.rbTodosProveedores);
            this.groupBox7.Controls.Add(this.rbSeleccionarProveedor);
            this.groupBox7.Controls.Add(this.btnCargarCotizaciones);
            this.groupBox7.Controls.Add(this.cbxProveedoresCotizacion);
            this.groupBox7.Location = new System.Drawing.Point(3, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(585, 63);
            this.groupBox7.TabIndex = 18;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Filtro";
            // 
            // btnCargarCotizaciones
            // 
            this.btnCargarCotizaciones.Location = new System.Drawing.Point(431, 22);
            this.btnCargarCotizaciones.Name = "btnCargarCotizaciones";
            this.btnCargarCotizaciones.Size = new System.Drawing.Size(136, 23);
            this.btnCargarCotizaciones.TabIndex = 19;
            this.btnCargarCotizaciones.Text = "Cargar Cotizaciones";
            this.btnCargarCotizaciones.UseVisualStyleBackColor = true;
            this.btnCargarCotizaciones.Click += new System.EventHandler(this.BtnCargarCotizaciones_Click);
            // 
            // rbTodosProveedores
            // 
            this.rbTodosProveedores.AutoSize = true;
            this.rbTodosProveedores.Checked = true;
            this.rbTodosProveedores.Location = new System.Drawing.Point(17, 25);
            this.rbTodosProveedores.Name = "rbTodosProveedores";
            this.rbTodosProveedores.Size = new System.Drawing.Size(134, 17);
            this.rbTodosProveedores.TabIndex = 21;
            this.rbTodosProveedores.TabStop = true;
            this.rbTodosProveedores.Text = "Todos los Proveedores";
            this.rbTodosProveedores.UseVisualStyleBackColor = true;
            this.rbTodosProveedores.CheckedChanged += new System.EventHandler(this.RbTodosProveedores_CheckedChanged);
            // 
            // rbSeleccionarProveedor
            // 
            this.rbSeleccionarProveedor.AutoSize = true;
            this.rbSeleccionarProveedor.Location = new System.Drawing.Point(157, 25);
            this.rbSeleccionarProveedor.Name = "rbSeleccionarProveedor";
            this.rbSeleccionarProveedor.Size = new System.Drawing.Size(74, 17);
            this.rbSeleccionarProveedor.TabIndex = 20;
            this.rbSeleccionarProveedor.Text = "Proveedor";
            this.rbSeleccionarProveedor.UseVisualStyleBackColor = true;
            // 
            // PantallaInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 612);
            this.Controls.Add(this.tabControl1);
            this.Name = "PantallaInicio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PantallaInicio";
            this.Load += new System.EventHandler(this.PantallaInicio_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTablaProductos)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabCRUD.ResumeLayout(false);
            this.tabCrearCotizacion.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgDetalleOrdenCompra)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabVerCotizaciones.ResumeLayout(false);
            this.tabDetalleOC.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTablaCotizaciones)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCrearProducto;
        private System.Windows.Forms.TextBox txtDescripcionProducto;
        private System.Windows.Forms.TextBox txtIdProducto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.DataGridView dgTablaProductos;
        private System.Windows.Forms.RadioButton rbBuscarId;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbBuscarDescripcion;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabCRUD;
        private System.Windows.Forms.TabPage tabCrearCotizacion;
        private System.Windows.Forms.DataGridView dgDetalleOrdenCompra;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnNuevaCotizacion;
        private System.Windows.Forms.TextBox txtDV;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtRazonSocial;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabVerCotizaciones;
        private System.Windows.Forms.Button btnPrevisualizarCotizacion;
        private System.Windows.Forms.Button btnFinalizarCotizacion;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnAgregarACotizacion;
        private System.Windows.Forms.Button btnNuevoProveedor;
        private System.Windows.Forms.ComboBox cbxProveedores;
        private System.Windows.Forms.Button btnModificarProveedor;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnCargarDatosProveedor;
        private System.Windows.Forms.TextBox txtRut;
        private System.Windows.Forms.TabPage tabDetalleOC;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton rbTodosProveedores;
        private System.Windows.Forms.RadioButton rbSeleccionarProveedor;
        private System.Windows.Forms.Button btnCargarCotizaciones;
        private System.Windows.Forms.ComboBox cbxProveedoresCotizacion;
        private System.Windows.Forms.Button btnEliminarCotizacion;
        private System.Windows.Forms.Button btnVerCotizacion;
        private System.Windows.Forms.DataGridView dgTablaCotizaciones;
    }
}

