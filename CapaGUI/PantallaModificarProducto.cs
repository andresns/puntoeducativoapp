﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaGUI.WebServicePuntoEducativo;

namespace CapaGUI
{
    public partial class PantallaModificarProducto : Form
    {
        private WebServicePuntoEducativo.Producto producto;
        private PantallaInicio pInicio;

        public Producto Producto { get => producto; set => producto = value; }
        public PantallaInicio PInicio { get => pInicio; set => pInicio = value; }

        public PantallaModificarProducto(WebServicePuntoEducativo.Producto producto, PantallaInicio pInicio)
        {
            InitializeComponent();
            this.Producto = producto;
            this.PInicio = pInicio;
        }

        private void PantallaModificarProducto_Load(object sender, EventArgs e)
        {
            txtIdProducto.Text = this.Producto.Id_producto.ToString();
            txtDescripcionProducto.Text = this.Producto.Descripcion;
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
            System.GC.Collect();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();

            WebServicePuntoEducativo.Producto auxProducto = new WebServicePuntoEducativo.Producto();
            auxProducto.Id_producto = int.Parse(txtIdProducto.Text);
            auxProducto.Descripcion = txtDescripcionProducto.Text;

            auxWSPuntoEducativo.modificarProducto(auxProducto);

            MessageBox.Show("Se han guardado los cambios.", "Sistema");

            this.Dispose();
            this.PInicio.cargarTablaProductos();
            System.GC.Collect();
        }
    }
}
