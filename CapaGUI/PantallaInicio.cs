﻿using CapaGUI.WebServicePuntoEducativo;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaGUI
{
    public partial class PantallaInicio : Form
    {
        private string codigoCotizacion;

        public string CodigoCotizacion { get => codigoCotizacion; set => codigoCotizacion = value; }

        public PantallaInicio()
        {
            InitializeComponent();
        }

        private void PantallaInicio_Load(object sender, EventArgs e)
        {
            btnModificar.Enabled = false;
            btnEliminar.Enabled = false;

            cargarTablaProductos();
            cargarProveedores();
        }

        #region CRUD

        public void cargarTablaProductos()
        {
            DataTable auxDataTable = new DataTable();
            auxDataTable.Columns.Add("Id", typeof(int));
            auxDataTable.Columns.Add("Descripcion", typeof(string));

            dgTablaProductos.DataSource = auxDataTable;

            WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();
            List<WebServicePuntoEducativo.Producto> auxListaProductos = new List<WebServicePuntoEducativo.Producto>();

            auxListaProductos = auxWSPuntoEducativo.obtenerProductos().ToList();

            foreach (WebServicePuntoEducativo.Producto p in auxListaProductos)
            {
                DataRow auxDataRow = auxDataTable.NewRow();
                auxDataRow["Id"] = p.Id_producto;
                auxDataRow["Descripcion"] = p.Descripcion;
                auxDataTable.Rows.Add(auxDataRow);
            }

            dgTablaProductos.Columns["Id"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgTablaProductos.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgTablaProductos.Columns["Id"].ReadOnly = true;
            dgTablaProductos.Columns["Descripcion"].ReadOnly = true;

            dgTablaProductos.ClearSelection();
        }
        private void BtnCrearProducto_Click(object sender, EventArgs e)
        {
            if (txtIdProducto.Text == string.Empty || txtDescripcionProducto.Text == string.Empty)
            {
                MessageBox.Show("Debe completar todos los campos", "Sistema");
            }
            else if (int.Parse(txtIdProducto.Text) < 1)
            {
                MessageBox.Show("El Id del producto debe ser mayor a 0", "Sistema");
            }
            else
            {
                WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();

                WebServicePuntoEducativo.Producto auxProducto = new WebServicePuntoEducativo.Producto();
                auxProducto.Id_producto = int.Parse(txtIdProducto.Text);
                auxProducto.Descripcion = txtDescripcionProducto.Text;
                auxWSPuntoEducativo.ingresarProducto(auxProducto);
                cargarTablaProductos();

                MessageBox.Show("El producto ha sido creado", "Sistema");
            }
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();
                List<WebServicePuntoEducativo.Producto> auxListaProductos = new List<WebServicePuntoEducativo.Producto>();

                if (rbBuscarId.Checked)
                {
                    if (txtBuscar.Text == string.Empty)
                    {
                        MessageBox.Show("Debe ingresar un Id para buscar", "Sistema");
                        return;
                    }
                    else
                    {
                        WebServicePuntoEducativo.Producto auxProducto = new WebServicePuntoEducativo.Producto();
                        auxProducto = auxWSPuntoEducativo.buscarProductoId(int.Parse(txtBuscar.Text));
                        auxListaProductos.Add(auxProducto);
                    }

                }
                else
                {
                    auxListaProductos = auxWSPuntoEducativo.buscarProductoDescripcion(txtBuscar.Text).ToList();
                }

                if (auxListaProductos.Count > 0)
                {
                    DataTable auxDataTable = new DataTable();
                    auxDataTable.Columns.Add("Id", typeof(int));
                    auxDataTable.Columns.Add("Descripcion", typeof(string));

                    dgTablaProductos.DataSource = auxDataTable;

                    foreach (WebServicePuntoEducativo.Producto p in auxListaProductos)
                    {
                        DataRow auxDataRow = auxDataTable.NewRow();
                        auxDataRow["Id"] = p.Id_producto;
                        auxDataRow["Descripcion"] = p.Descripcion;
                        auxDataTable.Rows.Add(auxDataRow);
                    }


                    dgTablaProductos.Columns["Id"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                    dgTablaProductos.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    dgTablaProductos.Columns["Id"].ReadOnly = true;
                    dgTablaProductos.Columns["Descripcion"].ReadOnly = true;

                    dgTablaProductos.ClearSelection();
                }
                else
                {
                    MessageBox.Show("No hay productos que coincidan con esa descripción", "Sistema");
                    cargarTablaProductos();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Sistema");
                cargarTablaProductos();
            }

        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            int indiceFila = dgTablaProductos.SelectedCells[0].RowIndex;
            DataGridViewRow filaSeleccionada = dgTablaProductos.Rows[indiceFila];

            int idProducto = int.Parse(filaSeleccionada.Cells["Id"].Value.ToString());
            string descProducto = filaSeleccionada.Cells["Descripcion"].Value.ToString();

            WebServicePuntoEducativo.Producto auxProducto = new WebServicePuntoEducativo.Producto();

            auxProducto.Id_producto = idProducto;
            auxProducto.Descripcion = descProducto;

            PantallaModificarProducto pModificarProducto = new PantallaModificarProducto(auxProducto, this);
            pModificarProducto.ShowDialog();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("¿Está seguro de eliminar el producto?", "Confirmación", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                int indiceFila = dgTablaProductos.SelectedCells[0].RowIndex;
                DataGridViewRow filaSeleccionada = dgTablaProductos.Rows[indiceFila];

                int idProducto = int.Parse(filaSeleccionada.Cells["Id"].Value.ToString());
                string descProducto = filaSeleccionada.Cells["Descripcion"].Value.ToString();

                WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();

                auxWSPuntoEducativo.eliminarProducto(idProducto);
                cargarTablaProductos();

                MessageBox.Show("El Producto ha sido eliminado.", "Sistema");
            }
        }

        #region Eventos GUI

        private void DgTablaResultados_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnModificar.Enabled = true;
            btnEliminar.Enabled = true;
        }

        private void TxtIdProducto_Enter(object sender, EventArgs e)
        {
            dgTablaProductos.ClearSelection();
        }

        private void TxtDescripcionProducto_Enter(object sender, EventArgs e)
        {
            dgTablaProductos.ClearSelection();
        }

        private void TxtBuscarProducto_Enter(object sender, EventArgs e)
        {
            dgTablaProductos.ClearSelection();
        }

        #endregion  Eventos GUI

        #endregion CRUD

        #region CrearCotizacion

        private void limpiarCampos()
        {
            txtRut.Text = string.Empty;
            txtDV.Text = string.Empty;
            txtRazonSocial.Text = string.Empty;
            txtDireccion.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            txtEmail.Text = string.Empty;
        }

        public void cargarProveedores()
        {
            WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();
            List<WebServicePuntoEducativo.Proveedor> auxListaProveedores = new List<WebServicePuntoEducativo.Proveedor>();

            auxListaProveedores = auxWSPuntoEducativo.obtenerProveedores().ToList();

            cbxProveedores.DisplayMember = "NombreProveedor";
            cbxProveedores.DataSource = auxListaProveedores;
            cbxProveedoresCotizacion.DisplayMember = "NombreProveedor";
            cbxProveedoresCotizacion.DataSource = auxListaProveedores;
        }

        private void BtnNuevoProveedor_Click(object sender, EventArgs e)
        {
            PantallaNuevoProveedor pNuevoProveedor = new PantallaNuevoProveedor(this);
            pNuevoProveedor.ShowDialog();
        }

        private void BtnNuevaCotizacion_Click(object sender, EventArgs e)
        {
            btnPrevisualizarCotizacion.Enabled = true;
            btnFinalizarCotizacion.Enabled = true;
            WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();
            WebServicePuntoEducativo.CabeceraCotizacion auxCabeceraCotizacion = new WebServicePuntoEducativo.CabeceraCotizacion();

            this.CodigoCotizacion = txtRut.Text.Replace(".", "") + DateTime.Now.ToString("yyMMddHHmmss");
            auxCabeceraCotizacion.CodigoCotizacion = this.CodigoCotizacion;
            auxCabeceraCotizacion.Fecha = DateTime.Now;
            auxCabeceraCotizacion.RunProveedor = txtRut.Text + "-" + txtDV.Text;

            auxWSPuntoEducativo.ingresarCabeceraCotizacion(auxCabeceraCotizacion);


            DataTable auxDataTable = new DataTable();
            auxDataTable.Columns.Add("Id", typeof(int));
            auxDataTable.Columns.Add("Descripcion", typeof(string));
            auxDataTable.Columns.Add("Cantidad", typeof(int));

            dgDetalleOrdenCompra.DataSource = auxDataTable;

            List<WebServicePuntoEducativo.DetalleOrdenCompra> auxDetalleOrdenCompras = new List<WebServicePuntoEducativo.DetalleOrdenCompra>();

            auxDetalleOrdenCompras = auxWSPuntoEducativo.obtenerDetalleOrdenCompras().ToList();

            foreach (WebServicePuntoEducativo.DetalleOrdenCompra doc in auxDetalleOrdenCompras)
            {
                WebServicePuntoEducativo.Producto auxProducto = new WebServicePuntoEducativo.Producto();

                auxProducto = auxWSPuntoEducativo.buscarProductoId(doc.IdProducto);
                DataRow auxDataRow = auxDataTable.NewRow();
                auxDataRow["Id"] = doc.IdProducto;
                auxDataRow["Descripcion"] = auxProducto.Descripcion;
                auxDataRow["Cantidad"] = doc.Cantidad;
                auxDataTable.Rows.Add(auxDataRow);
            }

            dgDetalleOrdenCompra.Columns["Id"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgDetalleOrdenCompra.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgDetalleOrdenCompra.Columns["Id"].ReadOnly = true;
            dgDetalleOrdenCompra.Columns["Descripcion"].ReadOnly = true;
            dgDetalleOrdenCompra.Columns["Cantidad"].ReadOnly = true;

            dgDetalleOrdenCompra.ClearSelection();

        }

        private void BtnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarCampos();
        }

        private void DgDetalleOrdenCompra_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnAgregarACotizacion.Enabled = true;
        }

        private void BtnAgregarACotizacion_Click(object sender, EventArgs e)
    {
            WebServicePuntoEducativo.DetalleCotizacion auxDetalleCotizacion = new WebServicePuntoEducativo.DetalleCotizacion();
            List<WebServicePuntoEducativo.DetalleCotizacion> auxListaDetalleCotizaciones = new List<WebServicePuntoEducativo.DetalleCotizacion>();


            int indiceFila = dgDetalleOrdenCompra.SelectedCells[0].RowIndex;
            DataGridViewRow filaSeleccionada = dgDetalleOrdenCompra.Rows[indiceFila];

            int idProducto = int.Parse(filaSeleccionada.Cells["Id"].Value.ToString());

            auxDetalleCotizacion.IdProducto = idProducto;
            auxDetalleCotizacion.CodCotizacion = this.codigoCotizacion;

            PantallaAgregarDetalleCotizacion pAgregarDetalleCotizacion = new PantallaAgregarDetalleCotizacion(this, auxDetalleCotizacion);
            pAgregarDetalleCotizacion.ShowDialog();
        }

        private void BtnFinalizarCotizacion_Click(object sender, EventArgs e)
        {

            cbxProveedores.SelectedIndex = -1;
            limpiarCampos();
            btnPrevisualizarCotizacion.Enabled = false;

            DataTable auxDataTable = new DataTable();
            dgDetalleOrdenCompra.DataSource = auxDataTable;
            dgTablaProductos.ClearSelection();

            this.CodigoCotizacion = "";

            MessageBox.Show("La cotización ha sido generada exitosamente.");
        }

        private void BtnPrevisualizarCotizacion_Click(object sender, EventArgs e)
        {
            PantallaPrevisualizarCotizacion pPrevisualizarCotizacion = new PantallaPrevisualizarCotizacion(this.CodigoCotizacion);
            pPrevisualizarCotizacion.ShowDialog();
        }

        #endregion CrearCotizacion

        #region Proveedor

        private void BtnCargarDatosProveedor_Click(object sender, EventArgs e)
        {
            WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();
            WebServicePuntoEducativo.Proveedor auxProveedor = new WebServicePuntoEducativo.Proveedor();

            auxProveedor = (WebServicePuntoEducativo.Proveedor)cbxProveedores.SelectedItem;

            string[] rutProveedor = auxProveedor.RunProveedor.Split('-');
            txtRut.Text = rutProveedor[0];
            txtDV.Text = rutProveedor[1];
            txtRazonSocial.Text = auxProveedor.NombreProveedor;
            txtDireccion.Text = auxProveedor.DireccionProveedor;
            txtTelefono.Text = auxProveedor.FonoProveedor.ToString();
            txtEmail.Text = auxProveedor.EmailProveedor;

            btnModificarProveedor.Enabled = true;
            btnNuevaCotizacion.Enabled = true;
        }

        private void BtnModificarProveedor_Click(object sender, EventArgs e)
        {
            if (btnModificarProveedor.Text == "Modificar Datos")
            {
                btnModificarProveedor.Text = "Guardar";
                txtRazonSocial.Enabled = true;
                txtDireccion.Enabled = true;
                txtTelefono.Enabled = true;
                txtEmail.Enabled = true;

            }
            else
            {
                btnModificarProveedor.Text = "Modificar Datos";

                WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();
                WebServicePuntoEducativo.Proveedor auxProveedor = new WebServicePuntoEducativo.Proveedor();

                auxProveedor.RunProveedor = txtRut.Text.Replace(',', '.') + "-" + txtDV.Text;
                auxProveedor.NombreProveedor = txtRazonSocial.Text;
                auxProveedor.DireccionProveedor = txtDireccion.Text;
                auxProveedor.FonoProveedor = txtTelefono.Text;
                auxProveedor.EmailProveedor = txtEmail.Text;

                auxWSPuntoEducativo.modificarProveedor(auxProveedor);

                cargarProveedores();

                txtRazonSocial.Enabled = false;
                txtDireccion.Enabled = false;
                txtTelefono.Enabled = false;
                txtEmail.Enabled = false;

                MessageBox.Show("Datos actualizados");
            }


        }

        #endregion Proveedor

        #region VerCotizaciones

        public void cargarTablaCotizaciones(int filtro, string rutProveedor)
        {
            WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();
            List<WebServicePuntoEducativo.CabeceraCotizacion> auxListaCotizaciones = new List<WebServicePuntoEducativo.CabeceraCotizacion>();

            if (filtro == 1)
            {
                //Filtrar por proveedor
                auxListaCotizaciones = auxWSPuntoEducativo.obtenerCabeceraCotizacionesProveedor(rutProveedor).ToList();
            }
            else
            {
                //Mostrar todas las cotizaciones
                auxListaCotizaciones = auxWSPuntoEducativo.obtenerCabeceraCotizaciones().ToList();
            }

            DataTable auxDataTable = new DataTable();
            auxDataTable.Columns.Add("Codigo Cotizacion", typeof(string));
            auxDataTable.Columns.Add("Proveedor", typeof(string));
            auxDataTable.Columns.Add("Fecha", typeof(DateTime));

            dgTablaCotizaciones.DataSource = auxDataTable;

            foreach (WebServicePuntoEducativo.CabeceraCotizacion cc in auxListaCotizaciones)
            {
                WebServicePuntoEducativo.Proveedor auxProveedor = new WebServicePuntoEducativo.Proveedor();

                auxProveedor = auxWSPuntoEducativo.obtenerProveedorRut(cc.RunProveedor);

                DataRow auxDataRow = auxDataTable.NewRow();
                auxDataRow["Codigo Cotizacion"] = cc.CodigoCotizacion;
                auxDataRow["Proveedor"] = auxProveedor.NombreProveedor;
                auxDataRow["Fecha"] = cc.Fecha;
                auxDataTable.Rows.Add(auxDataRow);
            }

            dgTablaCotizaciones.Columns["Codigo Cotizacion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgTablaCotizaciones.Columns["Proveedor"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgTablaCotizaciones.Columns["Codigo Cotizacion"].ReadOnly = true;
            dgTablaCotizaciones.Columns["Proveedor"].ReadOnly = true;
            dgTablaCotizaciones.Columns["Fecha"].ReadOnly = true;

            dgTablaCotizaciones.ClearSelection();
        }

        private void BtnCargarCotizaciones_Click(object sender, EventArgs e)
        {
            btnEliminarCotizacion.Enabled = true;
            btnVerCotizacion.Enabled = true;

            int filtro = 0;
            string rutProveedor = "";

            if (rbSeleccionarProveedor.Checked)
            {
                filtro = 1;
                WebServicePuntoEducativo.Proveedor auxProveedor = new WebServicePuntoEducativo.Proveedor();
                auxProveedor = (WebServicePuntoEducativo.Proveedor)cbxProveedoresCotizacion.SelectedItem;

                rutProveedor = auxProveedor.RunProveedor;
            }

            cargarTablaCotizaciones(filtro, rutProveedor);

        }

        private void BtnEliminarCotizacion_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("¿Está seguro de que desea eliminar la cotizacion?", "Confirmación", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();

                int indiceFila = dgTablaCotizaciones.SelectedCells[0].RowIndex;
                DataGridViewRow filaSeleccionada = dgTablaCotizaciones.Rows[indiceFila];

                string codigoCotizacion = filaSeleccionada.Cells["Codigo Cotizacion"].Value.ToString();

                auxWSPuntoEducativo.eliminarCabeceraCotizacion(codigoCotizacion);
                auxWSPuntoEducativo.eliminarDetalleCotizacionCodigo(codigoCotizacion);

                int filtro = 0;
                string rutProveedor = "";

                if (rbSeleccionarProveedor.Checked)
                {
                    filtro = 1;
                    WebServicePuntoEducativo.Proveedor auxProveedor = new WebServicePuntoEducativo.Proveedor();
                    auxProveedor = (WebServicePuntoEducativo.Proveedor)cbxProveedoresCotizacion.SelectedItem;

                    rutProveedor = auxProveedor.RunProveedor;
                }

                cargarTablaCotizaciones(filtro, rutProveedor);
            }
        }

        private void BtnVerCotizacion_Click(object sender, EventArgs e)
        {
            int indiceFila = dgTablaCotizaciones.SelectedCells[0].RowIndex;
            DataGridViewRow filaSeleccionada = dgTablaCotizaciones.Rows[indiceFila];

            string codigoCotizacion = filaSeleccionada.Cells["Codigo Cotizacion"].Value.ToString();

            PantallaPrevisualizarCotizacion pPrevisualizarCotizacion = new PantallaPrevisualizarCotizacion(codigoCotizacion);
            pPrevisualizarCotizacion.ShowDialog();
        }

        #endregion VerCotizaciones

        private void Button1_Click(object sender, EventArgs e)
        {
            WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();

            //auxWSPuntoEducativo.ingresarOrdenesCompraFecha(DateTime.Today);

            //DateTime fecha = DateTime.Today;
            //string fechaConsulta = fecha.ToString("ddMMyyyy");

            //string urlConsuta = "http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?fecha=" +fechaConsulta +"&CodigoProveedor=1263978&ticket=F8537A18-6766-4DEF-9E59-426B4FEE2844";
            //List<String> auxListaOrdenesCompra = new List<string>();

            //using (WebClient wc = new WebClient())
            //{
            //    var jsonString = wc.DownloadString(urlConsuta);

            //    JObject jObject = JObject.Parse(jsonString);

            //    JArray ordenesCompra = (JArray)jObject.SelectToken("Listado");
            //    foreach (JToken oc in ordenesCompra)
            //    {
            //        string codigoOrdenCompra = (string)oc.SelectToken("Codigo");
            //        auxListaOrdenesCompra.Add(codigoOrdenCompra);

            //    }
            //}


            //MessageBox.Show(auxListaOrdenesCompra[0]);
            //MessageBox.Show("Ya");



            //Obtener OC por fecha
            
            DateTime fecha = DateTime.Today;
            //string fechaConsulta = fecha.ToString("ddMMyyyy");
            string fechaConsulta = "01072019";
            string urlConsuta = "http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?fecha=" + fechaConsulta + "&CodigoProveedor=1263978&ticket=F8537A18-6766-4DEF-9E59-426B4FEE2844";

            List<String> auxListaOrdenesCompra = new List<string>();

            using (WebClient wc = new WebClient())
            {
                var jsonString = wc.DownloadString(urlConsuta);

                JObject jObject = JObject.Parse(jsonString);

                JArray ordenesCompra = (JArray)jObject.SelectToken("Listado");
                foreach (JToken oc in ordenesCompra)
                {
                    string codigoOrdenCompra = (string)oc.SelectToken("Codigo");
                    auxListaOrdenesCompra.Add(codigoOrdenCompra);
                }
            }//Fin obtener lista de ordenes de compra

            System.Threading.Thread.Sleep(5000);

            foreach (string oc in auxListaOrdenesCompra)
            {
                using (WebClient wc = new WebClient())
                {
                    System.Threading.Thread.Sleep(5000);
                    string urlConsulta = "http://api.mercadopublico.cl/servicios/v1/publico/ordenesdecompra.json?codigo=" + oc + "&ticket=F8537A18-6766-4DEF-9E59-426B4FEE2844";
                    var jsonString = wc.DownloadString(urlConsulta);

                    JObject jObject = JObject.Parse(jsonString);

                    WebServicePuntoEducativo.CabeceraOrdenCompra auxCabeceraOrdenCompra = new WebServicePuntoEducativo.CabeceraOrdenCompra();

                    //Obtenemos Cabecera OC
                    auxCabeceraOrdenCompra.CodigoOrdenCompra = (string)jObject.SelectToken("Listado[0].Codigo");
                    auxCabeceraOrdenCompra.RunCliente = (string)jObject.SelectToken("Listado[0].Comprador.RutUnidad");
                    auxCabeceraOrdenCompra.NombreCliente = (string)jObject.SelectToken("Listado[0].Comprador.NombreOrganismo");
                    auxCabeceraOrdenCompra.DireccionCliente = (string)jObject.SelectToken("Listado[0].Comprador.DireccionUnidad") + ", " + (string)jObject.SelectToken("Listado[0].Comprador.ComunaUnidad");
                    auxCabeceraOrdenCompra.FonoCliente = (string)jObject.SelectToken("Listado[0].Comprador.FonoContacto");

                    auxCabeceraOrdenCompra.EmailCliente = (string)jObject.SelectToken("Listado[0].Comprador.MailContacto");

                    string auxFechaOC = (string)jObject.SelectToken("FechaCreacion");
                    auxCabeceraOrdenCompra.Fecha = Convert.ToDateTime(auxFechaOC);

                    //Ingresamos cabeceras a Base de datos
                    auxWSPuntoEducativo.ingresarCabeceraOrdenCompra(auxCabeceraOrdenCompra);



                    //Obtenemos Detalle OC
                    JArray productos = (JArray)jObject.SelectToken("Listado[0].Items.Listado");

                    foreach (JToken p in productos)
                    {
                        string idProducto = (string)p.SelectToken("CodigoProducto");
                        string descripcion = (string)p.SelectToken("EspecificacionComprador");
                        int cantidad = (int)p.SelectToken("Cantidad");
                        int precioUnitario = (int)p.SelectToken("PrecioNeto");

                        //Ingresamos el producto a base de datos
                        WebServicePuntoEducativo.Producto auxProducto = new WebServicePuntoEducativo.Producto();

                        auxProducto.Id_producto = int.Parse(idProducto);
                        auxProducto.Descripcion = descripcion;

                        auxWSPuntoEducativo.ingresarProducto(auxProducto);

                        WebServicePuntoEducativo.DetalleOrdenCompra auxDetalleOrdenCompra = new WebServicePuntoEducativo.DetalleOrdenCompra();

                        auxDetalleOrdenCompra.Cantidad = cantidad;
                        auxDetalleOrdenCompra.PrecioUnitario = precioUnitario;
                        auxDetalleOrdenCompra.IdProducto = int.Parse(idProducto);
                        auxDetalleOrdenCompra.CodigoOrdenCompra = auxCabeceraOrdenCompra.CodigoOrdenCompra;

                        auxWSPuntoEducativo.ingresarDetalleOrdenCompra(auxDetalleOrdenCompra);

                    }
                }
            }

            MessageBox.Show("Ya");
            


        }

        private void RbTodosProveedores_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTodosProveedores.Checked)
            {
                cbxProveedoresCotizacion.Enabled = false;
            }
            else
            {
                cbxProveedoresCotizacion.Enabled = true;
            }
            
        }
    }
}
