﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaGUI
{
    public partial class PantallaNuevoProveedor : Form
    {
        private PantallaInicio pInicio;

        public PantallaNuevoProveedor(PantallaInicio pInicio)
        {
            InitializeComponent();
            this.PInicio = pInicio;
        }

        public PantallaInicio PInicio { get => pInicio; set => pInicio = value; }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
            System.GC.Collect();
        }

        private void BtnCrearProveedor_Click(object sender, EventArgs e)
        {
            WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();
            WebServicePuntoEducativo.Proveedor auxProveedor = new WebServicePuntoEducativo.Proveedor();

            auxProveedor.RunProveedor = txtRut.Text.Replace(',', '.') + "-" + txtDV.Text;
            auxProveedor.NombreProveedor = txtRazonSocial.Text;
            auxProveedor.DireccionProveedor = txtDireccion.Text;
            auxProveedor.FonoProveedor = txtTelefono.Text;
            auxProveedor.EmailProveedor = txtEmail.Text;

            auxWSPuntoEducativo.ingresarProveedor(auxProveedor);

            MessageBox.Show("Se ha creado el proveedor", "Sistema");

            this.Dispose();
            this.PInicio.cargarProveedores();
            System.GC.Collect();
        }
    }
}
