﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaGUI.WebServicePuntoEducativo;

namespace CapaGUI
{
    public partial class PantallaAgregarDetalleCotizacion : Form
    {
        private WebServicePuntoEducativo.DetalleCotizacion detalleCotizacion;
        private PantallaInicio pInicio;
        public DetalleCotizacion DetalleCotizacion { get => detalleCotizacion; set => detalleCotizacion = value; }
        public PantallaInicio PInicio { get => pInicio; set => pInicio = value; }

        public PantallaAgregarDetalleCotizacion(PantallaInicio pInicio, WebServicePuntoEducativo.DetalleCotizacion detalleCotizacion )
        {
            InitializeComponent();
            this.DetalleCotizacion = detalleCotizacion;
            this.PInicio = pInicio;
        }
        
        private void BtnAgregarDetalleCotizacion_Click(object sender, EventArgs e)
        {
            int num1, num2;
            bool resPrecio = Int32.TryParse(txtPrecio.Text, out num1);
            bool resCantidad = Int32.TryParse(txtCantidad.Text, out num2);
            if (resPrecio && resCantidad)
            {
                DetalleCotizacion.Cantidad = int.Parse(txtCantidad.Text);
                DetalleCotizacion.PrecioUnitario = int.Parse(txtPrecio.Text);

                WebServicePuntoEducativo.WebServicePESoapClient auxWSPuntoEducativo = new WebServicePuntoEducativo.WebServicePESoapClient();

                auxWSPuntoEducativo.ingresarDetalleCotizacion(this.DetalleCotizacion);

                MessageBox.Show("Se ha agregado el producto a la cotización.", "Sistema");

                this.Dispose();
                System.GC.Collect();
            }
            else
            {
                MessageBox.Show("Debe ingresar sólo valores numéricos para el precio y cantidad.");
            }
        }
    }
}
